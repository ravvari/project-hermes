package com.cccis.nitro.PubSubMessage.activemq;

import java.util.Map;



public interface ActiveMQMultiConfigHolder {
    Map<String, ActiveMQConfig> getActiveMQConnections();
}