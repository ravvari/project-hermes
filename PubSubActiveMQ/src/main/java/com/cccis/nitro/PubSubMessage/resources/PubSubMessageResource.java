/**
 * 
 */
/**
 * @author svalluri
 *
 */
package com.cccis.nitro.PubSubMessage.resources;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.cccis.nitro.PubSubMessage.activemq.ActiveMQSender;
import com.cccis.nitro.PubSubMessage.data.Message;
import com.cccis.nitro.PubSubMessage.utils.JSONUtils;
import com.cccis.nitro.PubSubMessage.utils.PubSubMessageResponse;
import com.fasterxml.jackson.databind.ObjectMapper;

@Path("telematicsMessage")
public class PubSubMessageResource {
	private Map<String, ActiveMQSender> sender = new HashMap<>(5);

	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Response sendMessage(String message) {
		PubSubMessageResponse response = new PubSubMessageResponse();

		if (!JSONUtils.isJSONValid(message)) {

			response.setSuccess(false);
			response.setMessage("Error in processing the request : Invalid JSON");

			return Response.status(500).entity(response).build();

		}

		

		// Based on topic ID get the topic name . for now it is trip_results
		// Send it via activeMq
		ObjectMapper mapper = new ObjectMapper();
		Message msg = null;
		try {
			msg = mapper.readValue(message, Message.class);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		sender.getOrDefault("topic:trip_results", sender.values().iterator().next()).send(msg);
		response.setSuccess(true);
		response.setMessage("Message sent via ActiveMQ : topic:trip_results -> " + message);
		return Response.status(200).entity(response).build();
	}

	public void addSender(String qName, ActiveMQSender activeMQSender) {
		sender.put(qName, activeMQSender);
	}
	
	
}