package com.cccis.nitro.PubSubMessage.activemq;

public interface ActiveMQReceiver<T> {

    public void receive(T message);
}
