package com.cccis.nitro.PubSubMessage.utils;

import java.io.Serializable;

public class PubSubMessageResponse implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public PubSubMessageResponse() {
		// TODO Auto-generated constructor stub
	}

	private boolean success;
	private String message;

	public boolean isSuccess() {
		return success;
	}

	public void setSuccess(boolean success) {
		this.success = success;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

}
