/**
 * 
 */
/**
 * @author svalluri
 *
 */
package com.cccis.nitro.PubSubMessage.errors;
public class JsonError extends RuntimeException {

    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public JsonError(Throwable cause) {
        super(cause);
    }
}

