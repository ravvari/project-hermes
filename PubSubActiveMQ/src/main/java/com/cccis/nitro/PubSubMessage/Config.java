package com.cccis.nitro.PubSubMessage;

import io.dropwizard.Configuration;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import com.cccis.nitro.PubSubMessage.activemq.ActiveMQConfig;
import com.cccis.nitro.PubSubMessage.activemq.ActiveMQMultiConfigHolder;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Map;

public class Config extends Configuration implements ActiveMQMultiConfigHolder {

    @JsonProperty
    @NotNull
    @Valid
    private Map<String, ActiveMQConfig> activeMQConnections;


    public Map<String, ActiveMQConfig> getActiveMQConnections() {
        return activeMQConnections;
    }
}