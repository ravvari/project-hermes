package com.cccis.nitro.PubSubMessage.data;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.common.base.MoreObjects;


public class Message {
	
	private String id;
	
	private String subject;
	
	private String message;
	
	private String topicId;
	
	private int priority;

	public Message() {
	}

	public Message(String id, String subject, String message, String topicId, int priority) {
		super();
		this.id = id;
		this.subject = subject;
		this.message = message;
		this.topicId = topicId;
		this.priority = priority;
	}

	public String getId() {
		return id;
	}

	public String getSubject() {
		return subject;
	}

	public String getMessage() {
		return message;
	}

	public String getTopicId() {
		return topicId;
	}

	public int getPriority() {
		return priority;
	}
	
	@Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
            .add("topicId", topicId)
            .add("message", message)
            .add("subject", subject)
            .toString();
    }

}
