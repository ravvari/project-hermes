package com.cccis.nitro.PubSubMessage.data;

import java.io.Serializable;
import java.util.List;
import java.util.UUID;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;

public class EventDataModel implements Serializable {

	public EventDataModel() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * Event data fields required for analysis
	 */
	private static final long serialVersionUID = 6992641759698340479L;

	@JsonProperty("messageId")
	@JsonInclude(Include.NON_NULL)
	private String messageId;

	@JsonProperty("deviceId")
	@JsonInclude(Include.NON_NULL)
	private String deviceId;

	@JsonProperty("ts")
	@JsonInclude(Include.NON_NULL)
	private String eventTimeStamp;

	@JsonProperty("vin")
	@JsonInclude(Include.NON_NULL)
	private String vin;

	@JsonProperty("speed")
	@JsonInclude(Include.NON_DEFAULT)
	private double speedMPH = 0.0;

	@JsonProperty("odometer")
	@JsonInclude(Include.NON_NULL)
	private String odameter;

	@JsonProperty("vehicleprotocol")
	@JsonInclude(Include.NON_NULL)
	private String vehicleProtocol;

	@JsonProperty("tripnumber")
	@JsonInclude(Include.NON_DEFAULT)
	private int tripNumber = 0;

	@JsonProperty("listOfPIDs")
	@JsonInclude(Include.NON_NULL)
	private List<Integer> listOfPIDsConfigured;

	@JsonProperty("EngineCoolantTemperature")
	@JsonInclude(Include.NON_DEFAULT)
	private double engineCoolantTemp = 0.0;

	@JsonProperty("EngineRPM")
	@JsonInclude(Include.NON_DEFAULT)
	private double engineRPMValue = 0.0;

	@JsonProperty("FuelLevelInput")
	@JsonInclude(Include.NON_DEFAULT)
	private double fuelLevelInput = 0.0;

	@JsonProperty("latitude")
	@JsonInclude(Include.NON_DEFAULT)
	private double latitudeGps = 0.0;

	@JsonProperty("LatitudeDirectionGPS")
	@JsonInclude(Include.NON_NULL)
	private String latitudeDirectionGps;

	@JsonProperty("longitude")
	@JsonInclude(Include.NON_DEFAULT)
	private double longitudeGps = 0.0;

	@JsonProperty("LongitudeDirectionGPS")
	@JsonInclude(Include.NON_NULL)
	private String longitudeDirectionGps;

	@JsonProperty("GPSCourseOverGround")
	@JsonInclude(Include.NON_DEFAULT)
	private int gpsCOG = 0;

	@JsonProperty("EventID")
	@JsonInclude(Include.ALWAYS)
	private String eventId = UUID.randomUUID().toString();

	@JsonProperty("IgnitionStatus")
	@JsonInclude(Include.NON_DEFAULT)
	private String ignitionStatus = "ON";

	@JsonProperty("IgnitionOffTime")
	@JsonInclude(Include.NON_DEFAULT)
	private String ignitionOffTime;

	@JsonProperty("gForceY")
	@JsonInclude(Include.NON_NULL)
	private double gForceY;

	@JsonProperty("gForceX")
	@JsonInclude(Include.NON_NULL)
	private double gForceX;

	@JsonProperty("IgnitionOnTime")
	@JsonInclude(Include.NON_NULL)
	private String ignitionOnTime;

	public String getMessageId() {
		return messageId;
	}

	public void setMessageId(String messageId) {
		this.messageId = messageId;
	}

	public String getDeviceId() {
		return deviceId;
	}

	public void setDeviceId(String deviceId) {
		this.deviceId = deviceId;
	}

	public String getVin() {
		return vin;
	}

	public void setVin(String vin) {
		this.vin = vin;
	}

	public double getSpeedMPH() {
		return speedMPH;
	}

	public void setSpeedMPH(double speedMPH) {
		this.speedMPH = speedMPH;
	}

	public String getOdameter() {
		return odameter;
	}

	public void setOdameter(String odameter) {
		this.odameter = odameter;
	}

	public String getVehicleProtocol() {
		return vehicleProtocol;
	}

	public void setVehicleProtocol(String vehicleProtocol) {
		this.vehicleProtocol = vehicleProtocol;
	}

	public int getTripNumber() {
		return tripNumber;
	}

	public void setTripNumber(int tripNumber) {
		this.tripNumber = tripNumber;
	}

	public List<Integer> getListOfPIDsConfigured() {
		return listOfPIDsConfigured;
	}

	public void setListOfPIDsConfigured(List<Integer> listOfPIDsConfigured) {
		this.listOfPIDsConfigured = listOfPIDsConfigured;
	}

	/**
	 * @return the engineCoolantTemp
	 */
	public double getEngineCoolantTemp() {
		return engineCoolantTemp;
	}

	/**
	 * @param engineCoolantTemp
	 *            the engineCoolantTemp to set
	 */
	public void setEngineCoolantTemp(double engineCoolantTemp) {
		this.engineCoolantTemp = engineCoolantTemp;
	}

	/**
	 * @return the engineRPMValue
	 */
	public double getEngineRPMValue() {
		return engineRPMValue;
	}

	/**
	 * @param engineRPMValue
	 *            the engineRPMValue to set
	 */
	public void setEngineRPMValue(double engineRPMValue) {
		this.engineRPMValue = engineRPMValue;
	}

	/**
	 * @return the fuelLevelInput
	 */
	public double getFuelLevelInput() {
		return fuelLevelInput;
	}

	/**
	 * @param fuelLevelInput
	 *            the fuelLevelInput to set
	 */
	public void setFuelLevelInput(double fuelLevelInput) {
		this.fuelLevelInput = fuelLevelInput;
	}

	/**
	 * @return the eventTimeStamp
	 */
	public String getEventTimeStamp() {
		return eventTimeStamp;
	}

	/**
	 * @param eventTimeStamp
	 *            the eventTimeStamp to set
	 */
	public void setEventTimeStamp(String eventTimeStamp) {
		this.eventTimeStamp = eventTimeStamp;
	}

	/**
	 * @return the latitudeGps
	 */
	public double getLatitudeGps() {
		return latitudeGps;
	}

	/**
	 * @param latitudeGps
	 *            the latitudeGps to set
	 */
	public void setLatitudeGps(double latitudeGps) {
		this.latitudeGps = latitudeGps;
	}

	/**
	 * @return the latitudeDirectionGps
	 */
	public String getLatitudeDirectionGps() {
		return latitudeDirectionGps;
	}

	/**
	 * @param latitudeDirectionGps
	 *            the latitudeDirectionGps to set
	 */
	public void setLatitudeDirectionGps(String latitudeDirectionGps) {
		this.latitudeDirectionGps = latitudeDirectionGps;
	}

	/**
	 * @return the longitudeGps
	 */
	public double getLongitudeGps() {
		return longitudeGps;
	}

	/**
	 * @param longitudeGps
	 *            the longitudeGps to set
	 */
	public void setLongitudeGps(double longitudeGps) {
		this.longitudeGps = longitudeGps;
	}

	/**
	 * @return the longitudeDirectionGps
	 */
	public String getLongitudeDirectionGps() {
		return longitudeDirectionGps;
	}

	/**
	 * @param longitudeDirectionGps
	 *            the longitudeDirectionGps to set
	 */
	public void setLongitudeDirectionGps(String longitudeDirectionGps) {
		this.longitudeDirectionGps = longitudeDirectionGps;
	}

	/**
	 * @return the gpsCOG
	 */
	public int getGpsCOG() {
		return gpsCOG;
	}

	/**
	 * @param gpsCOG
	 *            the gpsCOG to set
	 */
	public void setGpsCOG(int gpsCOG) {
		this.gpsCOG = gpsCOG;
	}

	/**
	 * @return the eventId
	 */
	public String getEventId() {
		return eventId;
	}

	/**
	 * @param eventId
	 *            the eventId to set
	 */
	public void setEventId(String eventId) {
		this.eventId = eventId;
	}

	

	/**
	 * @return the ignitionStatus
	 */
	public String getIgnitionStatus() {
		return ignitionStatus;
	}

	/**
	 * @param ignitionStatus the ignitionStatus to set
	 */
	public void setIgnitionStatus(String ignitionStatus) {
		this.ignitionStatus = ignitionStatus;
	}

	/**
	 * @return the gForceY
	 */
	public double getgForceY() {
		return gForceY;
	}

	/**
	 * @param gForceY
	 *            the gForceY to set
	 */
	public void setgForceY(double gForceY) {
		this.gForceY = gForceY;
	}

	/**
	 * @return the gForceX
	 */
	public double getgForceX() {
		return gForceX;
	}

	/**
	 * @param gForceX
	 *            the gForceX to set
	 */
	public void setgForceX(double gForceX) {
		this.gForceX = gForceX;
	}

	/**
	 * @return the ignitionOnTime
	 */
	public String getIgnitionOnTime() {
		return ignitionOnTime;
	}

	/**
	 * @param ignitionOnTime
	 *            the ignitionOnTime to set
	 */
	public void setIgnitionOnTime(String ignitionOnTime) {
		this.ignitionOnTime = ignitionOnTime;
	}

	/**
	 * @return the ignitionOffTime
	 */
	public String getIgnitionOffTime() {
		return ignitionOffTime;
	}

	/**
	 * @param ignitionOffTime
	 *            the ignitionOffTime to set
	 */
	public void setIgnitionOffTime(String IgnitionOffTime) {
		this.ignitionOffTime = IgnitionOffTime;
	}

	@Override
	public String toString() {
		return deviceId + "_" + messageId + "_" + eventTimeStamp;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((deviceId == null) ? 0 : deviceId.hashCode());
		result = prime * result + ((eventTimeStamp == null) ? 0 : eventTimeStamp.hashCode());
		result = prime * result + ((messageId == null) ? 0 : messageId.hashCode());
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		EventDataModel other = (EventDataModel) obj;
		if (deviceId == null) {
			if (other.deviceId != null)
				return false;
		} else if (!deviceId.equals(other.deviceId))
			return false;
		if (eventTimeStamp == null) {
			if (other.eventTimeStamp != null)
				return false;
		} else if (!eventTimeStamp.equals(other.eventTimeStamp))
			return false;
		if (messageId == null) {
			if (other.messageId != null)
				return false;
		} else if (!messageId.equals(other.messageId))
			return false;
		return true;
	}
}
