package com.cccis.nitro.PubSubMessage.activemq;

public interface ActiveMQConfigHolder {

    ActiveMQConfig getActiveMQ();
}