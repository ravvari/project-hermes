package com.cccis.nitro.PubSubMessage;

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.cccis.nitro.PubSubMessage.activemq.ActiveMQBundle;
import com.cccis.nitro.PubSubMessage.activemq.ActiveMQMultiBundle;
import com.cccis.nitro.PubSubMessage.data.Message;
import com.cccis.nitro.PubSubMessage.resources.PubSubMessageResource;

import io.dropwizard.Application;
import io.dropwizard.setup.Bootstrap;
import io.dropwizard.setup.Environment;

/**
 * 
 *
 */
public class PubSubMessageApplication extends Application<Config> {

    private final Logger log = LoggerFactory.getLogger(getClass());

    public static void main(String[] args) throws Exception {
        new PubSubMessageApplication().run(args);
    }

    private ActiveMQMultiBundle activeMQBundle;

    @Override
    public void initialize(Bootstrap<Config> configBootstrap) {

        // Create the bundle and store reference to it
        this.activeMQBundle = new ActiveMQMultiBundle();
        // Add the bundle
        configBootstrap.addBundle(activeMQBundle);
    }

    @Override
    public void run(Config config, Environment environment) throws Exception {
        PubSubMessageResource multiQResource = new PubSubMessageResource();
        environment.jersey().register(multiQResource);
        activeMQBundle.getActiveMQBundleMap().entrySet()
            .stream()
            .forEach(activeMQEntry->activateQueue(multiQResource, activeMQEntry));
     // Set up the sender for our queue
        //ActiveMQSender sender = activeMQBundle.createSender( "topic:trip_analyzer", false);


    }

    private void activateQueue(PubSubMessageResource multiQResource, Map.Entry<String, ActiveMQBundle> activeMQEntry) {
        String queueName = activeMQEntry.getKey();
        ActiveMQBundle activeMQBundle = activeMQEntry.getValue();

        // Set up the sender for our queue
        multiQResource.addSender(queueName, activeMQBundle.createSender( queueName, false));

        // Set up a receiver that just logs the messages we receive on our queue
        //Logic have to be added for multiple subscribers for single topic Same client
        activeMQBundle.registerReceiver(
            queueName,
            (message) -> log.info("\n*****\nWe received a message on: {} from activeMq: \n{}\n*****", queueName, message),
            Message.class,
            true);
    }
    
    
    
    

}