package com.cccis.nitro.hermes.data.model;

import java.io.Serializable;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * 
 * @author ravvari
 *
 */
public class SubscriptionDataModel implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2748872565363888873L;

	@JsonProperty("id")
	@JsonInclude(Include.ALWAYS)
	private String id;

	@JsonProperty("carrierId")
	@JsonInclude(Include.ALWAYS)
	private String carrierId;

	@JsonProperty("endpoints")
	@JsonInclude(Include.ALWAYS)
	private List<EndpointModel> endpoints;

	@JsonProperty("name")
	@JsonInclude(Include.ALWAYS)
	private String name;

	@JsonProperty("topicIds")
	@JsonInclude(Include.ALWAYS)
	private List<String> topicIds;

	@JsonProperty("active")
	@JsonInclude(Include.ALWAYS)
	private boolean active;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getCarrierId() {
		return carrierId;
	}

	public void setCarrierId(String carrierId) {
		this.carrierId = carrierId;
	}

	public List<EndpointModel> getEndpoints() {
		return endpoints;
	}

	public void setEndpoints(List<EndpointModel> endpoints) {
		this.endpoints = endpoints;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<String> getTopicIds() {
		return topicIds;
	}

	public void setTopicIds(List<String> topicIds) {
		this.topicIds = topicIds;
	}

	public boolean isActive() {
		return active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}
}
