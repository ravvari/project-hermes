package com.cccis.nitro.hermes.data.model;

import java.io.Serializable;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

/**
 * 
 * @author ravvari
 *
 */
public class OptInOptOutDataModel implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1073997031668421553L;

	@JsonProperty("enrollmentId")
	@JsonInclude(Include.ALWAYS)
	private String enrollmentId;

	@JsonProperty("carrierId")
	@JsonInclude(Include.ALWAYS)
	private String carrierId;

	@JsonProperty("deviceId")
	@JsonInclude(Include.ALWAYS)
	private String deviceId;

	@JsonProperty("deviceDetails")
	@JsonInclude(Include.ALWAYS)
	private List<DeviceDetails> deviceDetails;

	@JsonProperty("optin")
	@JsonInclude(Include.NON_EMPTY)
	private boolean optin;

	@JsonProperty("timestamp")
	@JsonInclude(Include.NON_NULL)
	private String timestamp;

	public String getEnrollmentId() {
		return enrollmentId;
	}

	public void setEnrollmentId(String enrollmentId) {
		this.enrollmentId = enrollmentId;
	}

	public String getCarrierId() {
		return carrierId;
	}

	public void setCarrierId(String carrierId) {
		this.carrierId = carrierId;
	}

	public String getDeviceId() {
		return deviceId;
	}

	public void setDeviceId(String deviceId) {
		this.deviceId = deviceId;
	}

	public List<DeviceDetails> getDeviceDetails() {
		return deviceDetails;
	}

	public void setDeviceDetails(List<DeviceDetails> deviceDetails) {
		this.deviceDetails = deviceDetails;
	}

	public boolean isOptin() {
		return optin;
	}

	public void setOptin(boolean optin) {
		this.optin = optin;
	}

	public String getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(String timestamp) {
		this.timestamp = timestamp;
	}

}
