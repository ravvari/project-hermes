package com.cccis.nitro.hermes.data.model;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

public class EndpointModel implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7091477896239995790L;

	@JsonProperty("type")
	@JsonInclude(Include.ALWAYS)
	private String type;

	@JsonProperty("value")
	@JsonInclude(Include.ALWAYS)
	private String value;

	public EndpointModel() {
		super();
	}

	public EndpointModel(String type, String value) {
		super();
		this.type = type;
		this.value = value;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}
}
