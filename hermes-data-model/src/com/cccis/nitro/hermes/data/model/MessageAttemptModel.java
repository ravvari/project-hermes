package com.cccis.nitro.hermes.data.model;

import java.io.Serializable;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

public class MessageAttemptModel implements Serializable {



	/**
	 * 
	 */
	private static final long serialVersionUID = 3461442141729072504L;

	@JsonProperty("messageId")
	@JsonInclude(Include.ALWAYS)
	private String messageId;
	
	@JsonProperty("endPoint")
	@JsonInclude(Include.ALWAYS)
	private EndpointModel endPoint;

	@JsonProperty("success")
	@JsonInclude(Include.ALWAYS)
	private boolean success;

	@JsonProperty("dispatchResult")
	@JsonInclude(Include.ALWAYS)
	private String dispatchResult;
	
	@JsonProperty("subject")
	@JsonInclude(Include.NON_DEFAULT)
	private String subject = "";
	
	@JsonProperty("message")
	@JsonInclude(Include.NON_DEFAULT)
	private String message = "";

	@JsonProperty("attempts")
	@JsonInclude(Include.ALWAYS)
	private int attempts = 0;
	
	@JsonProperty("attemptedTime")
	@JsonInclude(Include.ALWAYS)
	private long attemptedTime;
	
	//{"endPoint":{"type":"HTTP","value":"http://localhost:9080/receive-notifications"},"attemptedTime":1489773562,"success":false,"dispatchResult":"Message could not be sent. Retrying soon !","subject":"test-message","messageId":"f3fae97d-3fa5-47c8-8861-2b6abe9f66d2","message":"input-message","attempts":1}


	public String getMessageId() {
		return messageId;
	}

	public void setMessageId(String messageId) {
		this.messageId = messageId;
	}

	public boolean isSuccess() {
		return success;
	}

	public void setSuccess(boolean success) {
		this.success = success;
	}

	public String getDispatchResult() {
		return dispatchResult;
	}

	public void setDispatchResult(String dispatchResult) {
		this.dispatchResult = dispatchResult;
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public EndpointModel getEndPoint() {
		return endPoint;
	}

	public void setEndPoint(EndpointModel endPoint) {
		this.endPoint = endPoint;
	}

	public int getAttempts() {
		return attempts;
	}

	public void setAttempts(int attempts) {
		this.attempts = attempts;
	}

	public long getAttemptedTime() {
		return attemptedTime;
	}

	public void setAttemptedTime(long attemptedTime) {
		this.attemptedTime = attemptedTime;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((endPoint == null) ? 0 : endPoint.hashCode());
		result = prime * result + ((messageId == null) ? 0 : messageId.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		
		MessageAttemptModel other = (MessageAttemptModel) obj;
		if (endPoint == null) {
			if (other.endPoint != null)
				return false;
		} else if (!endPoint.getValue().equals(other.endPoint.getValue()))
			return false;
		if (messageId == null) {
			if (other.messageId != null)
				return false;
		} else if (!messageId.equals(other.messageId))
			return false;
		return true;
	}
}
