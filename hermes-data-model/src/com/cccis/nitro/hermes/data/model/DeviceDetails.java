package com.cccis.nitro.hermes.data.model;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

public class DeviceDetails implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8081296742671339717L;

	@JsonProperty("name")
	@JsonInclude(Include.ALWAYS)
	private String name;

	@JsonProperty("value")
	@JsonInclude(Include.ALWAYS)
	private String value;

	public DeviceDetails() {
		super();
	}

	public DeviceDetails(String name, String value) {
		super();
		this.name = name;
		this.value = value;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}
}
