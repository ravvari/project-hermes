package com.cccis.nitro.hermes.data.model;

import java.io.Serializable;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

/**
 * 
 * @author ravvari
 *
 */
public class TopicDataModel implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7727325675286368296L;

	@JsonProperty("id")
	@JsonInclude(Include.ALWAYS)
	private String id;

	@JsonProperty("carrierId")
	@JsonInclude(Include.ALWAYS)
	private String carrierId;

	@JsonProperty("name")
	@JsonInclude(Include.ALWAYS)
	private String name;

	@JsonProperty("description")
	@JsonInclude(Include.NON_NULL)
	private String description;

	@JsonProperty("active")
	@JsonInclude(Include.NON_NULL)
	private boolean active;

	@JsonProperty("features")
	@JsonInclude(Include.NON_NULL)
	private List<String> features;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getCarrierId() {
		return carrierId;
	}

	public void setCarrierId(String carrierId) {
		this.carrierId = carrierId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public boolean isActive() {
		return active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}

	public List<String> getFeatures() {
		return features;
	}

	public void setFeatures(List<String> features) {
		this.features = features;
	}

}
