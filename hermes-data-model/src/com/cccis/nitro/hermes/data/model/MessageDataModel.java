package com.cccis.nitro.hermes.data.model;

import java.io.Serializable;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

/**
 * 
 * @author ravvari
 *
 */
public class MessageDataModel implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2984449590037557523L;

	@JsonProperty("id")
	@JsonInclude(Include.ALWAYS)
	private String id;

	@JsonProperty("carrierId")
	@JsonInclude(Include.ALWAYS)
	private String carrierId;

	@JsonProperty("subject")
	@JsonInclude(Include.NON_DEFAULT)
	private String subject = "";

	@JsonProperty("message")
	@JsonInclude(Include.ALWAYS)
	private String message;

	@JsonProperty("features")
	@JsonInclude(Include.ALWAYS)
	private List<String> features;

	@JsonProperty("priority")
	@JsonInclude(Include.NON_DEFAULT)
	private int priority = 0;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getCarrierId() {
		return carrierId;
	}

	public void setCarrierId(String carrierId) {
		this.carrierId = carrierId;
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public List<String> getFeatures() {
		return features;
	}

	public void setFeatures(List<String> features) {
		this.features = features;
	}

	public int getPriority() {
		return priority;
	}

	public void setPriority(int priority) {
		this.priority = priority;
	}

}