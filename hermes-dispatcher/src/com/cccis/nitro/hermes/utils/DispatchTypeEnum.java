package com.cccis.nitro.hermes.utils;

public enum DispatchTypeEnum {
	
	HTTP("http"),
	EMAIL("email");
	
	private String type;

	DispatchTypeEnum(String type) {
        this.type = type;
    }

    public String type() {
        return type;
    }
}
