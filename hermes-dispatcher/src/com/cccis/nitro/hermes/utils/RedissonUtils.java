package com.cccis.nitro.hermes.utils;

import org.redisson.Config;
import org.redisson.Redisson;
import org.redisson.RedissonClient;
import org.redisson.codec.SerializationCodec;

public class RedissonUtils {

		protected static RedissonClient redisson;

		public RedissonClient getRedisson() {
			return redisson;
		}

		public Config createConfig(String[] redisIPs) {
						
			Config config = new Config();
			//config.useClusterServers().addNodeAddress(redisIPs);
			config.useElasticacheServers().addNodeAddress(redisIPs);
			config.setCodec(new SerializationCodec());
			return config;
		}

		public RedissonClient createInstance(String[] redisIPs) {
			redisson = Redisson.create(createConfig(redisIPs));
			return redisson;
		}

		void shutDown() {
			redisson.shutdown();
		}
}
