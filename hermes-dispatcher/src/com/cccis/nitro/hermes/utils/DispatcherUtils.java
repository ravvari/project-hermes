package com.cccis.nitro.hermes.utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.storm.Config;
import org.apache.storm.Constants;
import org.apache.storm.StormSubmitter;
import org.apache.storm.generated.AlreadyAliveException;
import org.apache.storm.generated.AuthorizationException;
import org.apache.storm.generated.InvalidTopologyException;
import org.apache.storm.generated.StormTopology;
import org.apache.storm.kafka.KafkaSpout;
import org.apache.storm.kafka.ZkHosts;
import org.apache.storm.tuple.Tuple;
import org.json.JSONObject;
import org.json.JSONTokener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.cccis.nitro.hermes.data.model.MessageAttemptModel;
import com.cccis.nitro.hermes.data.model.MessageDataModel;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.util.Properties;

/*
 * This class contains various general purpose utils methods
 */
public class DispatcherUtils {
	
	private static final Logger LOG = LoggerFactory.getLogger(DispatcherUtils.class);
	
	private ObjectMapper mapper = new ObjectMapper();

	/*Read and return the properties object for the given config file*/
	public Properties getPropValuesByPath(String configFilePath) throws IOException {
		 
		Properties prop = new Properties();

		InputStream inputStream = new FileInputStream(configFilePath);

		if (inputStream != null) {
			prop.load(inputStream);
		} 			
		return prop;
	}
	
	
	
	/*
	 * This code is used to determine true or false for Tuple is Tick_Tuple
	 */
	public static boolean isTickTuple(Tuple tuple) {
		  return tuple.getSourceComponent().equals(Constants.SYSTEM_COMPONENT_ID)
		    && tuple.getSourceStreamId().equals(Constants.SYSTEM_TICK_STREAM_ID);
	}
	
	public boolean postMessageToHTTPEndPoint(String endPoint, MessageDataModel message)
	{
		HttpClient httpClient = null;
    	HttpResponse response = null;
    	    	    	
    	httpClient = HttpClientBuilder.create().build();
    	
    	JSONObject messageJson = new JSONObject();
    	
    	if(message.getSubject() != null)
    	{
    		messageJson.put("subject", message.getSubject());
    	}
    	
    	messageJson.put("message", message.getMessage());

	    try
	    {
	        StringEntity params = message ==null?null:new StringEntity(messageJson.toString());

	        HttpPost request = new HttpPost(endPoint);

	        request.setEntity(params);
		    response = httpClient.execute(request);
		    
	    	if(null != response)
	    	{						    
			    if(response.getStatusLine().getStatusCode() != 200)
			    {
			    	LOG.error("Unable to dispatch message to endpoint "+endPoint);
			    	LOG.error(response.getStatusLine().getReasonPhrase());
			    	return false;
			    }
	    	}
	    }
	    catch(Exception e)
	    {
	    	LOG.error("Unable to dispatch message to endpoint "+endPoint);
	    	LOG.error(e.getLocalizedMessage());
	    	return false;
	    }
	    
		return true;
	}
	
	public KafkaSpout getKafkaSpout(String zooKeeperHost, String topicName, String spoutName) throws Exception{     
		   
		  
		  ZkHosts zkHost = new ZkHosts(zooKeeperHost);
		  		  		  		  
		  //Create a KafkaSpout for fetching the event streams
		  org.apache.storm.kafka.SpoutConfig spoutConfig = new org.apache.storm.kafka.SpoutConfig(zkHost,topicName,"/kafka",spoutName);
		  
		  spoutConfig.scheme = new org.apache.storm.spout.SchemeAsMultiScheme(new org.apache.storm.kafka.StringScheme());

		  //spoutConfig.scheme = new org.apache.storm.spout.SchemeAsMultiScheme(new org.apache.storm.kafka.StringScheme());
		  		  		  		  		  		  		  
		  KafkaSpout spout = new KafkaSpout(spoutConfig);
		  
		  return spout;
	  }

	public void runTopologyRemotely(StormTopology topology, String topologyName, Config conf)
		      throws AlreadyAliveException, InvalidTopologyException, AuthorizationException {
		    StormSubmitter.submitTopology(topologyName, conf, topology);
		  }

	public MessageDataModel deserializeMessageModelFromJSON(JSONObject messageJson) throws JsonParseException, JsonMappingException, IOException {
				   
		   String is = messageJson.toString();
				   
		   MessageDataModel messageModel =
			        mapper.readValue(is, MessageDataModel.class);
		   
		   return messageModel;
	}
	
	public MessageAttemptModel deserializeMessageAttemptModelFromJSON(JSONObject messageJson) throws JsonParseException, JsonMappingException, IOException {
		
		   String is = messageJson.toString();
				   
		   MessageAttemptModel messageAttemptModel =
			        mapper.readValue(is, MessageAttemptModel.class);
		   
		   return messageAttemptModel;
	}
}
