package com.cccis.nitro.hermes.bolt;

import java.util.List;
import java.util.Map;

import org.apache.storm.task.OutputCollector;
import org.apache.storm.task.TopologyContext;
import org.apache.storm.topology.OutputFieldsDeclarer;
import org.apache.storm.topology.base.BaseRichBolt;
import org.apache.storm.tuple.Fields;
import org.apache.storm.tuple.Tuple;
import org.apache.storm.tuple.Values;

import com.cccis.nitro.hermes.data.model.EndpointModel;
import com.cccis.nitro.hermes.data.model.MessageAttemptModel;
import com.cccis.nitro.hermes.data.model.MessageDataModel;
import com.cccis.nitro.hermes.utils.DispatchTypeEnum;
import com.cccis.nitro.hermes.utils.DispatcherUtils;

public class RetryFailedMessageBolt extends BaseRichBolt{

	/**
	 * 
	 */
	
	private OutputCollector collector;

	private static final long serialVersionUID = -3862629606496700916L;
	
	private DispatcherUtils utils;
	
	private int maxRetryCount;
	
	private long diffInSeconds;
	
	public RetryFailedMessageBolt(String maxRetryCount, String diffInSeconds)
	{
		this.maxRetryCount = Integer.parseInt(maxRetryCount);
		this.diffInSeconds = Long.parseLong(diffInSeconds);
	}

	@Override
	public void prepare(Map stormConf, TopologyContext context, OutputCollector collector) {
		// TODO Auto-generated method stub
		this.collector = collector;
		this.utils = new DispatcherUtils();
	}

	@SuppressWarnings("unchecked")
	@Override
	public void execute(Tuple input) {
		collector.ack(input);
		
		if(DispatcherUtils.isTickTuple(input)){
			return;
		} 
		else
		{			
			MessageAttemptModel messageAttModel = (MessageAttemptModel) input.getValueByField("failed_message");

			if(!(Math.abs(messageAttModel.getAttemptedTime()-System.currentTimeMillis()/1000)>900))
			{
				return;
			}
			
			MessageDataModel message = new MessageDataModel();
			
			message.setSubject(messageAttModel.getSubject());
			
			message.setMessage(messageAttModel.getMessage());
			
			EndpointModel endPoint = messageAttModel.getEndPoint();
			
			String type = endPoint.getType();
			
			if(messageAttModel.getAttempts() >= maxRetryCount)
			{
				messageAttModel.setSubject(message.getSubject());
				messageAttModel.setMessage(message.getMessage());
				messageAttModel.setDispatchResult("Message could not be sent");

				collector.emit(new Values(messageAttModel,false));
			}
				
			messageAttModel.setAttempts(messageAttModel.getAttempts()+1);
				
			boolean dispatchStatus = false;
				
			if(type.equalsIgnoreCase(DispatchTypeEnum.HTTP.type()))
			{
				dispatchStatus = utils.postMessageToHTTPEndPoint(endPoint.getValue(), message);				
			}
			else if(type.equalsIgnoreCase(DispatchTypeEnum.EMAIL.type()))
			{
					
			}

			if(dispatchStatus)
			{					
				messageAttModel.setDispatchResult("Message sent successfully !");
			}
			else
			{
				messageAttModel.setSubject(message.getSubject());
				messageAttModel.setMessage(message.getMessage());
				messageAttModel.setDispatchResult("Message could not be sent. Retrying soon !");
			}
			
			messageAttModel.setAttemptedTime(System.currentTimeMillis()/1000);
			
			collector.emit(new Values(messageAttModel,dispatchStatus));
			
		}
		
		
	}

	@Override
	public void declareOutputFields(OutputFieldsDeclarer declarer) {
		declarer.declare(new Fields("message_attempt_model","status"));
	}

}
