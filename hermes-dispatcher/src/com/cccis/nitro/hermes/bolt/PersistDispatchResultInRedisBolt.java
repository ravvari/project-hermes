package com.cccis.nitro.hermes.bolt;

import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import org.apache.storm.task.OutputCollector;
import org.apache.storm.task.TopologyContext;
import org.apache.storm.topology.OutputFieldsDeclarer;
import org.apache.storm.topology.base.BaseRichBolt;
import org.apache.storm.tuple.Fields;
import org.apache.storm.tuple.Tuple;
import org.apache.storm.tuple.Values;
import org.redisson.RedissonClient;
import org.redisson.core.RMapCache;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.cccis.nitro.hermes.data.model.MessageAttemptModel;
import com.cccis.nitro.hermes.utils.DispatcherUtils;
import com.cccis.nitro.hermes.utils.RedissonUtils;


public class PersistDispatchResultInRedisBolt extends BaseRichBolt {
	/**
	 * 
	 */
	private static final long serialVersionUID = -8446593798833024283L;

	private OutputCollector collector;
    private RedissonClient redisson;
    
    private RedissonUtils redissonUtils;
        
    private String redisIPList;
    
    private int redisExpiryMinutes;
    
    private static final Logger LOG = LoggerFactory.getLogger(PersistDispatchResultInRedisBolt.class);
    
    
    public PersistDispatchResultInRedisBolt(String redisIPList, String redisExpiryMinutes)
    {
    	this.redisIPList = redisIPList;
    	this.redisExpiryMinutes = Integer.parseInt(redisExpiryMinutes);
    }
    
	@Override
	public void prepare(Map stormConf, TopologyContext context, OutputCollector collector) {
		this.collector = collector;
		this.redissonUtils = new RedissonUtils();
	    redisson = redissonUtils.createInstance(redisIPList.split(","));
	}

	@Override
	public void execute(Tuple input) {
		
		collector.ack(input);
				
		if(DispatcherUtils.isTickTuple(input)){
			return;
		} else {

		RMapCache<String,List<MessageAttemptModel>> messageStatusCache = redisson.getMapCache("messageStatusCache");
		
		MessageAttemptModel attemptedMessage = (MessageAttemptModel)input.getValueByField("message_attempt_model");
		
		List<MessageAttemptModel> messageAttemptList = new LinkedList<MessageAttemptModel>();
	
		try
		{			
			if(!messageStatusCache.containsKey(attemptedMessage.getMessageId()))
			{
				messageStatusCache.put(attemptedMessage.getMessageId(), messageAttemptList,redisExpiryMinutes,TimeUnit.MINUTES);
			}
			else
			{
				messageAttemptList = messageStatusCache.get(attemptedMessage.getMessageId());
				
				int index = messageAttemptList.indexOf(attemptedMessage);

				if(index >= 0)
				{
					messageAttemptList.add(index, attemptedMessage);
				}
				else
				{
					messageAttemptList.add(attemptedMessage);
				}
			}
			
			collector.emit(new Values(attemptedMessage,attemptedMessage.isSuccess()));
		}
		catch(Exception e)
		{
			e.printStackTrace();
			LOG.error("Exception occurred in the PersistDispatchResult bolt "+e.getMessage());
		}
		
	  }
		
	}

	@Override
	public void declareOutputFields(OutputFieldsDeclarer declarer) {
        declarer.declare(new Fields("message_attempt","status"));
	}
}
