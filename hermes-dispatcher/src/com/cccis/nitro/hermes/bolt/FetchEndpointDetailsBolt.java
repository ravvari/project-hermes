package com.cccis.nitro.hermes.bolt;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import org.apache.storm.shade.org.eclipse.jetty.io.EndPoint;
import org.apache.storm.task.OutputCollector;
import org.apache.storm.task.TopologyContext;
import org.apache.storm.topology.OutputFieldsDeclarer;
import org.apache.storm.topology.base.BaseRichBolt;
import org.apache.storm.tuple.Fields;
import org.apache.storm.tuple.Tuple;
import org.apache.storm.tuple.Values;
import org.redisson.RedissonClient;
import org.redisson.core.RMap;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.cccis.nitro.hermes.data.model.EndpointModel;
import com.cccis.nitro.hermes.data.model.MessageDataModel;
import com.cccis.nitro.hermes.utils.DispatcherUtils;
import com.cccis.nitro.hermes.utils.RedissonUtils;

public class FetchEndpointDetailsBolt extends BaseRichBolt {
	/**
	 * 
	 */
	private static final long serialVersionUID = 2680547731445894572L;
	/**
	 * 
	 */

	private OutputCollector collector;
    private RedissonClient redisson;
    
    private RedissonUtils redissonUtils;
            
    private String redisIPList;
    
    private static final Logger LOG = LoggerFactory.getLogger(FetchEndpointDetailsBolt.class);
    
    
    public FetchEndpointDetailsBolt(String redisIPList)
    {
    	this.redisIPList = redisIPList;
    }
    
	@Override
	public void prepare(Map stormConf, TopologyContext context, OutputCollector collector) {
		this.collector = collector;
		this.redissonUtils = new RedissonUtils();
	    this.redisson = redissonUtils.createInstance(redisIPList.split(","));
	}

	@Override
	public void execute(Tuple input) {
		
		collector.ack(input);
				
		if(DispatcherUtils.isTickTuple(input)){
			return;
		}
		
		RMap<String, List<EndpointModel>> topicSubscriberMap = redisson.getMap("topicSubscriberEndPoint");
		
	
		try
		{
			MessageDataModel message = (MessageDataModel)input.getValueByField("message");
			
			List<EndpointModel> endPointList = new ArrayList<EndpointModel>();
			
			for(String feature : message.getFeatures())
			{			
				if(topicSubscriberMap.containsKey(message.getCarrierId()+"_"+feature))
				{					
					endPointList.addAll(topicSubscriberMap.get(message.getCarrierId()+"_"+feature));
				}
			}

			collector.emit(new Values(message,endPointList));					
		}
		catch(Exception e)
		{
			e.printStackTrace();
			LOG.error("Exception occurred while fetching endpoint detailss "+e.getMessage());
		}
		
	}

	@Override
	public void declareOutputFields(OutputFieldsDeclarer declarer) {
        declarer.declare(new Fields("message","endpoints"));
	}
}
