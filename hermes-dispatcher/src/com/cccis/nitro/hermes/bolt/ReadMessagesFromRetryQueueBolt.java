package com.cccis.nitro.hermes.bolt;

import java.util.Map;

import org.apache.storm.task.OutputCollector;
import org.apache.storm.task.TopologyContext;
import org.apache.storm.topology.OutputFieldsDeclarer;
import org.apache.storm.topology.base.BaseRichBolt;
import org.apache.storm.tuple.Fields;
import org.apache.storm.tuple.Tuple;
import org.apache.storm.tuple.Values;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.cccis.nitro.hermes.data.model.MessageAttemptModel;
import com.cccis.nitro.hermes.utils.DispatcherUtils;

public class ReadMessagesFromRetryQueueBolt extends BaseRichBolt {
	/**
	 * 
	 */
	private static final long serialVersionUID = -7738992840065377300L;
	/**
	 * Reads and Emits the message data model
	 */

    private static final Logger log = LoggerFactory.getLogger(ReadMessagesFromRetryQueueBolt.class);
	
	private OutputCollector collector;
	
	private DispatcherUtils utils;

	@Override
	public void prepare(Map stormConf, TopologyContext context, OutputCollector collector) {
		this.collector = collector;
		this.utils = new DispatcherUtils();
	}

	@Override
	public void execute(Tuple input) {
		collector.ack(input);
		
		if(DispatcherUtils.isTickTuple(input))
		{
			return;
		}

			MessageAttemptModel messageAttemptModel = null;
			
			JSONObject json =new JSONObject(input.getString(0));
			
			try
			{
				messageAttemptModel = utils.deserializeMessageAttemptModelFromJSON(json);
			}
			catch(Exception e)
			{
				log.error("Problem encountered while deserializing the message");
				log.error(e.getLocalizedMessage());
			}
							
			collector.emit(new Values(messageAttemptModel));
		
	}

	@Override
	public void declareOutputFields(OutputFieldsDeclarer declarer) {
		declarer.declare(new Fields("failed_message"));
	}
}
