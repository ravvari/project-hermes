package com.cccis.nitro.hermes.bolt;

import java.util.List;
import java.util.Map;

import org.apache.storm.task.OutputCollector;
import org.apache.storm.task.TopologyContext;
import org.apache.storm.topology.OutputFieldsDeclarer;
import org.apache.storm.topology.base.BaseRichBolt;
import org.apache.storm.tuple.Fields;
import org.apache.storm.tuple.Tuple;
import org.apache.storm.tuple.Values;

import com.cccis.nitro.hermes.data.model.EndpointModel;
import com.cccis.nitro.hermes.data.model.MessageAttemptModel;
import com.cccis.nitro.hermes.data.model.MessageDataModel;
import com.cccis.nitro.hermes.utils.DispatchTypeEnum;
import com.cccis.nitro.hermes.utils.DispatcherUtils;

public class MessageDispatchBolt extends BaseRichBolt{

	/**
	 * 
	 */
	
	private OutputCollector collector;

	private static final long serialVersionUID = -3862629606496700916L;
	
	private DispatcherUtils utils;

	@Override
	public void prepare(Map stormConf, TopologyContext context, OutputCollector collector) {
		// TODO Auto-generated method stub
		this.collector = collector;
		this.utils = new DispatcherUtils();
	}

	@SuppressWarnings("unchecked")
	@Override
	public void execute(Tuple input) {
		collector.ack(input);
		
		if(DispatcherUtils.isTickTuple(input)){
			return;
		} 
		else
		{
			List<EndpointModel> endPoints = (List<EndpointModel>)input.getValueByField("endpoints");
			
			MessageDataModel message = (MessageDataModel) input.getValueByField("message");
			
			for(EndpointModel endPoint : endPoints)
			{
				String type = endPoint.getType();
				
				MessageAttemptModel messageAttempt = new MessageAttemptModel();				
				
				messageAttempt.setMessageId(message.getId());
				
				messageAttempt.setEndPoint(endPoint);
				
				messageAttempt.setAttempts(messageAttempt.getAttempts()+1);
				
				boolean dispatchStatus = false;
				
				if(type.equalsIgnoreCase(DispatchTypeEnum.HTTP.type()))
				{
					dispatchStatus = utils.postMessageToHTTPEndPoint(endPoint.getValue(), message);				
				}
				else if(type.equalsIgnoreCase(DispatchTypeEnum.EMAIL.type()))
				{
					
				}
				
				if(dispatchStatus)
				{					
					messageAttempt.setDispatchResult("Message sent successfully !");
				}
				else
				{
					messageAttempt.setSubject(message.getSubject());
					messageAttempt.setMessage(message.getMessage());
					messageAttempt.setDispatchResult("Message could not be sent. Retrying soon !");
				}
				
				messageAttempt.setSuccess(dispatchStatus);
				
				messageAttempt.setAttemptedTime(System.currentTimeMillis()/1000);
				
				collector.emit(new Values(messageAttempt,dispatchStatus));
			}
		}
		
		
	}

	@Override
	public void declareOutputFields(OutputFieldsDeclarer declarer) {
		declarer.declare(new Fields("message_attempt_model","status"));
	}

}
