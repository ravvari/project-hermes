package com.cccis.nitro.hermes.bolt;

import java.util.Map;
import java.util.Properties;

import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.storm.kafka.bolt.KafkaBolt;
import org.apache.storm.task.OutputCollector;
import org.apache.storm.task.TopologyContext;
import org.apache.storm.topology.OutputFieldsDeclarer;
import org.apache.storm.tuple.Tuple;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.cccis.nitro.hermes.data.model.MessageAttemptModel;
import com.cccis.nitro.hermes.utils.DispatcherUtils;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;

public class RouteMessagesToQueueBolt<K,V> extends KafkaBolt<K, V> {

    /**
	 * 
	 */
	private static final long serialVersionUID = 6615489230372427435L;

	private static final Logger log = LoggerFactory.getLogger(RouteMessagesToQueueBolt.class);
	
	private OutputCollector collector;
			
	private Properties producerProperties;
	
	private String kafkaServer;
	
	private String kafkaRetryTopic;

	private String kafkaFailureTopic;
	
	private int maxRetryAttempt;
	
	public RouteMessagesToQueueBolt(String kafkaServer, String kafkaRetryTopic, String kafkaFailureTopic, String maxRetryAttempt)
	{
		this.kafkaServer = kafkaServer;
		this.kafkaRetryTopic = kafkaRetryTopic;
		this.kafkaFailureTopic = kafkaFailureTopic;
		this.maxRetryAttempt = Integer.parseInt(maxRetryAttempt);
	}

	@Override
	public void prepare(Map stormConf, TopologyContext context, OutputCollector collector) {
		this.collector = collector;
		this.producerProperties = new Properties();
		this.producerProperties.put("metadata.broker.list", kafkaServer);
		producerProperties.put("request.required.acks", "1");
		producerProperties.put("serializer.class","org.apache.kafka.common.serialization.StringSerializer");
		producerProperties.put("bootstrap.servers", kafkaServer);
		producerProperties.put("key.serializer","org.apache.kafka.common.serialization.StringSerializer");
		producerProperties.put("value.serializer","org.apache.kafka.common.serialization.StringSerializer");
	}

	@Override
	public void execute(Tuple input) {
		
		if(DispatcherUtils.isTickTuple(input)){
			return;
		}
		
		Boolean status = (Boolean)input.getValueByField("status");
		
		if(status)
			return;
		
		KafkaProducer<K, String> producer = new KafkaProducer<>(producerProperties);
		
		MessageAttemptModel messageAttModel = (MessageAttemptModel) input.getValueByField("message_attempt");
		
		ObjectMapper mapper = new ObjectMapper();
		mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, true);
		mapper.configure(DeserializationFeature.FAIL_ON_NULL_FOR_PRIMITIVES, false);
		mapper.configure(SerializationFeature.INDENT_OUTPUT, true);
		
		try
		{
			JSONObject messageAttJson = new JSONObject(mapper.writeValueAsString(messageAttModel));
	
			producer.send(new ProducerRecord<K,String>(messageAttModel.getAttempts()>=maxRetryAttempt?kafkaFailureTopic:kafkaRetryTopic,messageAttJson.toString()));
			
			collector.ack(input);
		}
		catch(Exception e)
		{
			e.printStackTrace();
			log.error("Exception occurred while pushing the message to kafka "+e.getMessage());
		}
		finally
		{
			producer.close();
		}
	}

	@Override
	public void declareOutputFields(OutputFieldsDeclarer declarer) {
	}
}
