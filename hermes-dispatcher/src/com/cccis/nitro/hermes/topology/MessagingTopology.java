package com.cccis.nitro.hermes.topology;

import java.util.Properties;

import org.apache.storm.kafka.KafkaSpout;
import org.apache.storm.topology.TopologyBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.cccis.nitro.hermes.bolt.FetchEndpointDetailsBolt;
import com.cccis.nitro.hermes.bolt.MessageDispatchBolt;
import com.cccis.nitro.hermes.bolt.PersistDispatchResultInRedisBolt;
import com.cccis.nitro.hermes.bolt.ReadMessagesFromQueueBolt;
import com.cccis.nitro.hermes.bolt.ReadMessagesFromRetryQueueBolt;
import com.cccis.nitro.hermes.bolt.RetryFailedMessageBolt;
import com.cccis.nitro.hermes.bolt.RouteMessagesToQueueBolt;
import com.cccis.nitro.hermes.utils.DispatcherUtils;

public class MessagingTopology {
	
	private static final Logger LOG = LoggerFactory.getLogger(MessagingTopology.class);


	public TopologyBuilder returnMessagingTopology(String zkHost, String kafkaServer, String redisIP, Properties topoProp)
	{				
		
		TopologyBuilder builder = new TopologyBuilder();
		
		DispatcherUtils utils = new DispatcherUtils();
								
		try
		{
			
			KafkaSpout messagesSpout = utils.getKafkaSpout(zkHost, topoProp.getProperty("input_messages_topic"),"input_messages");

			KafkaSpout failedMessagesSpout = utils.getKafkaSpout(zkHost, topoProp.getProperty("retry_messages_topic"),"retry_messages");

			builder.setSpout("input_messages", messagesSpout, 1);
		    
		    builder.setSpout("messages_to_be_retried", failedMessagesSpout, 1);
		    
			//Read & deserialize the messages 
		    builder.setBolt("read_messages", new ReadMessagesFromQueueBolt()).shuffleGrouping("input_messages");
		    
		    //Read & deserialize the failed messages
		    builder.setBolt("read_retryable_messages", new ReadMessagesFromRetryQueueBolt()).shuffleGrouping("messages_to_be_retried");
		    
		    //Fetch the endpoints for the given topic
		    builder.setBolt("fetch_endpoint_details", new FetchEndpointDetailsBolt(topoProp.getProperty("redisIPList"))).shuffleGrouping("read_messages");

		    //Dispatch the messages
		    builder.setBolt("dispatch_messages", new MessageDispatchBolt()).shuffleGrouping("fetch_endpoint_details");
		    
		    //Retry the failed messages
		    builder.setBolt("dispatch_failed_messages", new RetryFailedMessageBolt(topoProp.getProperty("max_retry_attempts"),topoProp.getProperty("min_retry_secs"))).shuffleGrouping("read_retryable_messages");
		    
		    //persist the message status
		    builder.setBolt("persist_message_status", new PersistDispatchResultInRedisBolt(redisIP,topoProp.getProperty("redis_expiry_hours")))
		    				.shuffleGrouping("dispatch_messages")
		    				.shuffleGrouping("dispatch_failed_messages");
		    
		    //Push messages to Kafka
		    builder.setBolt("send_results_to_queue", new RouteMessagesToQueueBolt<>(kafkaServer, topoProp.getProperty("retry_messages_topic"), topoProp.getProperty("failed_messages_topic"), topoProp.getProperty("max_retry_attempts")))
		    				.shuffleGrouping("persist_message_status");
     
		}
		catch(Exception e)
		{
			LOG.error("Exception occurred while running the messaging topology "+e.getMessage());
		}
		
		return builder;
	}
}
