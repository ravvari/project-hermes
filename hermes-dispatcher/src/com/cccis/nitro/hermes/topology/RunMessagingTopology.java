package com.cccis.nitro.hermes.topology;

import java.io.IOException;
import java.util.Properties;

import org.apache.storm.Config;
import org.apache.storm.LocalCluster;
import org.apache.storm.topology.TopologyBuilder;

import com.cccis.nitro.hermes.utils.DispatcherUtils;

public class RunMessagingTopology {

	public static void main(String args[])
	{
		
		DispatcherUtils utils = new DispatcherUtils();
				
		MessagingTopology messagingTopology = new MessagingTopology();
		
    	try
    	{
			
    		Properties runTopoProp = utils.getPropValuesByPath(args[0]);
			
			String zkHost = runTopoProp.getProperty("kafkaZKHost");
			
			String kafkaServer = runTopoProp.getProperty("kafkaBrokerList");
			
			String localMode = runTopoProp.getProperty("localMode");
			
			String redisIPList = runTopoProp.getProperty("redisIPList");
						
			TopologyBuilder messagingTopo = messagingTopology.returnMessagingTopology(zkHost, kafkaServer, redisIPList, runTopoProp);

			Config conf = new Config();
			
			conf.put(Config.TOPOLOGY_DEBUG, false);
						
			conf.setDebug(false);
			
			if(localMode.equalsIgnoreCase("false"))
			{
				utils.runTopologyRemotely(messagingTopo.createTopology(), "messaging-topology", conf);
			}
			else
			{
				LocalCluster cluster = new LocalCluster();
	            cluster.submitTopology("messaging-topology", conf, messagingTopo.createTopology());
	        }
    	}
    	catch(IOException e)
    	{
    		throw new IllegalArgumentException(
                    "The path must point to a valid local filesystem path. filename=[{}].".replace("{}", args[0]));
    	}
    	catch(Exception e)
    	{
    		e.printStackTrace();
    	}
	}
}
