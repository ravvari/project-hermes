package com.cccis.nitro.HermesMessaging.resources;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.cccis.nitro.HermesMessaging.utils.JSONUtils;
import com.cccis.nitro.HermesMessaging.utils.KafkaProducer;
import com.cccis.nitro.HermesMessaging.utils.HermesMessagingResponse;

/**
 * 
 * @author ravvari
 *
 */
@Path("/messages")
@Produces(MediaType.APPLICATION_JSON)
public class MessagesResource {

	private final String topicName;
	private final String uri;

	public MessagesResource(String topicName, String uri) {
		super();
		this.topicName = topicName;
		this.uri = uri;
	}

	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Response sendMessage(String message, @Context HttpServletRequest request) {
		HermesMessagingResponse response = new HermesMessagingResponse();
		KafkaProducer kafkaProducer = new KafkaProducer();
		try {
			if (!JSONUtils.isJSONValid(message)) {
				response.setSuccess(false);
				response.setMessage("Error in processing the request : Invalid JSON");
				return Response.status(500).entity(response).build();
			}
			kafkaProducer.publishMesssage(message, topicName, uri);
			response.setSuccess(true);
			response.setMessage("Message successfully posted");
			return Response.status(201).entity(response).build();
		} catch (Exception e) {
			return Response.status(500).entity(e.getMessage()).build();
		}
	}

}