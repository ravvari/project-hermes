package com.cccis.nitro.HermesMessaging.utils;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

public class HermesMessagingResponse implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public HermesMessagingResponse() {
		// TODO Auto-generated constructor stub
	}

	@JsonInclude(Include.ALWAYS)
	private boolean success;

	@JsonInclude(Include.ALWAYS)
	private String message;

	@JsonInclude(Include.NON_NULL)
	private Object content;

	public boolean isSuccess() {
		return success;
	}

	public void setSuccess(boolean success) {
		this.success = success;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public Object getContent() {
		return content;
	}

	public void setContent(Object content) {
		this.content = content;
	}

	public HermesMessagingResponse(boolean success, String message, Object content) {
		super();
		this.success = success;
		this.message = message;
		this.content = content;
	}

}
