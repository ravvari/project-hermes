package com.cccis.nitro.HermesMessaging.utils;

import java.util.Properties;


import kafka.javaapi.producer.Producer;
import kafka.producer.KeyedMessage;
import kafka.producer.ProducerConfig;


public class KafkaProducer {
	   
	   private Producer<Integer, String> producer;

       public void initialize(String url) {
             Properties producerProps = new Properties();
             producerProps.put("metadata.broker.list", url);
             producerProps.put("serializer.class", "kafka.serializer.StringEncoder");
             producerProps.put("request.required.acks", "1");
             ProducerConfig producerConfig = new ProducerConfig(producerProps);
             producer = new Producer<Integer, String>(producerConfig);        
       }
       
       public void publishMesssage(String msgToBePublished,String topic, String url) throws Exception{     
    	   
    	 initialize(url);
    	 
         String msg = msgToBePublished;
         //Define topic name and message
         KeyedMessage<Integer, String> keyedMsg =
                        new KeyedMessage<Integer, String>(topic, msg);
         
         producer.send(keyedMsg); // This publishes message on given topic
         System.out.println("--> Message [" + msg + "] sent.Check message on Consumer's program console");
         return;
       }
       
       public void closeTunnel()
       {
    	   producer.close();
       }
}