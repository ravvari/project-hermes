package com.cccis.nitro.HermesMessaging.resources;

import java.io.IOException;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.cccis.nitro.HermesMessaging.utils.JSONUtils;
import com.cccis.nitro.HermesMessaging.utils.PersisterUtils;
import com.cccis.nitro.hermes.data.model.TopicDataModel;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.cccis.nitro.HermesMessaging.utils.HermesMessagingResponse;

@Path("/topics")
@Produces(MediaType.APPLICATION_JSON)
public class TopicsResource {

	private static final String TOPIC_DATA_MODEL = "topic-data-model";
	private PersisterUtils persisterUtils;
	private static final String QUERY_TOPIC_BY_ID = "{\"from\":[\"topic-data-model\"],\"limit\":[{\"id\":{\"eq\":\"%s\"}}]}";

	private static final Logger LOG = LoggerFactory.getLogger(TopicsResource.class);

	public TopicsResource() throws Exception {
		super();
		this.persisterUtils = new PersisterUtils();
	}

	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	public Response createTopic(String topic, @Context HttpServletRequest request) {
		HermesMessagingResponse response = new HermesMessagingResponse();
		String daasResponseUrl;
		JSONObject jsonTopic;
		ObjectMapper mapper = new ObjectMapper();
		if (!JSONUtils.isJSONValid(topic)) {
			response.setSuccess(false);
			response.setMessage("Error in processing the request : Invalid JSON");
			return Response.status(500).entity(response).build();
		}

		String topicId = UUID.randomUUID().toString();
		TopicDataModel topicDetails = new TopicDataModel();

		try {
			topicDetails = mapper.readValue(topic, TopicDataModel.class);
			topicDetails.setId(topicId);
			jsonTopic = new JSONObject(topicDetails);
			LOG.info("Received topic data :" + topic);

		} catch (IOException e1) {
			e1.printStackTrace();
			response.setSuccess(false);
			response.setMessage("Error in processing the request : Invalid topic fields");
			return Response.status(500).entity(response).build();
		}

		try {
			daasResponseUrl = persisterUtils.persistData(TOPIC_DATA_MODEL, jsonTopic.toString());
			topicDetails = (TopicDataModel) persisterUtils.getData(daasResponseUrl, TopicDataModel.class);
			System.out.println("resp: " + topicDetails.getId());
			System.out.println("persisted topic : " + jsonTopic.toString());
		} catch (Exception e) {
			String error = "Error in persisting topic :" + jsonTopic.toString();
			LOG.error(error);
			System.out.println(error);
			response.setSuccess(false);
			response.setMessage(error);
			return Response.status(500).entity(response).build();
		}

		response.setSuccess(true);
		response.setMessage("Topic created with given details");
		response.setContent(topicDetails);
		return Response.status(200).entity(response).build();
	}

	@GET
	@Path("/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getTopic(@PathParam("id") String id) {
		HermesMessagingResponse response = new HermesMessagingResponse();
		String query = String.format(QUERY_TOPIC_BY_ID, id);
		TopicDataModel topicDetails = new TopicDataModel();
		try {
			topicDetails = (TopicDataModel) persisterUtils.queryDataModelByCriteria(query, TopicDataModel.class);
		} catch (Exception e) {
			LOG.error("Error in retrieving topic with id: " + id);
			System.out.println("Error in retrieving topic with id: " + id);
		}

		response.setSuccess(true);
		response.setMessage("Topic details successfully retrieved");
		response.setContent(topicDetails);
		return Response.status(200).entity(response).build();
	}

	@PUT
	@Path("/{id}")
	@Consumes(MediaType.APPLICATION_JSON)
	public Response updateTopic(@PathParam("id") String id, String updatedTopicDetails,
			@Context HttpServletRequest request) {
		HermesMessagingResponse response = new HermesMessagingResponse();
		String query = String.format(QUERY_TOPIC_BY_ID, id);
		if (!JSONUtils.isJSONValid(updatedTopicDetails)) {
			response.setSuccess(false);
			response.setMessage("Error in processing the request : Invalid JSON");
			return Response.status(500).entity(response).build();
		}
		try {
			LOG.info("Received update request for topic with data :" + updatedTopicDetails);
			persisterUtils.updateData(TOPIC_DATA_MODEL, query, updatedTopicDetails, TopicDataModel.class);
			System.out.println("Updated topic : " + updatedTopicDetails);
		} catch (Exception e) {
			String error = "Error in updating topic :" + updatedTopicDetails;
			LOG.error(error);
			response.setSuccess(false);
			response.setMessage(error);
			System.out.println(error);

		}

		response.setSuccess(true);
		response.setMessage("Topic updated with given details");
		return Response.status(200).entity(response).build();

	}
}