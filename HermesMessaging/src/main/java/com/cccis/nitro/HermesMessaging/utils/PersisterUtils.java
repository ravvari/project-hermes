package com.cccis.nitro.HermesMessaging.utils;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;

import org.apache.http.client.ClientProtocolException;
import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Properties;
import java.util.UUID;

import com.cccis.nitro.persistence.client.exceptions.PersistDataException;
import com.cccis.nitro.persistence.client.params.InsertRecordParams;
import com.cccis.nitro.persistence.client.params.SelectRecordByCriteriaParams;
import com.cccis.nitro.persistence.client.params.UpdateRecordParams;
import com.cccis.nitro.persistence.client.rest.PersistenceClient;
import com.cccis.nitro.persistence.client.support.PersistenceConfig;
import com.fasterxml.jackson.databind.ObjectMapper;

/*
 * This class contains various general purpose util methods
 */
public class PersisterUtils {

	private static final Logger LOG = LoggerFactory.getLogger(PersisterUtils.class);

	private static PersistenceClient daas = null;
	private static PersistenceConfig config = null;

	public PersisterUtils() throws Exception {
		try {
			daas = new PersistenceClient();
		} catch (IOException e) {
			e.printStackTrace();
			throw new Exception("Could not create Daas PersistenceClient:" + e.getLocalizedMessage());
		}
		config = daas.getConfig();
	}

	@SuppressWarnings("unchecked")
	public String persistData(String modelType, String dataString) {

		String urlResponse = null;
		String sessionInfo = UUID.randomUUID().toString();

		@SuppressWarnings({ "rawtypes" })
		InsertRecordParams insertParams = new InsertRecordParams(dataString.getClass(), sessionInfo,
				config.persistConnInfo.host, config.url_insertRecord, config.appName, modelType);
		try {
			insertParams.setPersistData(dataString);

			urlResponse = daas.insertRecord(config, insertParams);
		} catch (PersistDataException | NullPointerException | IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		LOG.info("Persisted in " + modelType + " : " + urlResponse);
		System.out.println("Persisted in " + modelType + " : " + urlResponse);
		return urlResponse;
	}

	public <T> boolean updateData(String modelType, String query, String dataString, Class<T> class1) {

		boolean updateResult = false;

		String sessionInfo = UUID.randomUUID().toString();
		SelectRecordByCriteriaParams params = new SelectRecordByCriteriaParams(sessionInfo, config.persistConnInfo.host,
				config.url_selectCriteria, config.appName);
		params.setQuery(query);

		String result, recordId = null;

		try {
			result = daas.selectRecordbyCriteria(config, params);
			if (result != null) {
				JSONObject resultJson = new JSONObject(result);
				Boolean success = resultJson.getBoolean("success");
				if (success) {
					// get logical data records
					JSONObject records = resultJson.getJSONObject("dataset");
					JSONArray jsonArray = records.getJSONArray("_logical_data_records");
					JSONObject dataFromDaas = (JSONObject) jsonArray.get(0);
					recordId = dataFromDaas.getString("recordId");
					// dataFromDaas = removeDaasFields(dataFromDaas);
					// ObjectMapper objectMapper = new ObjectMapper();
					// String dataFromDaasString = dataFromDaas.toString();
					// resultData = objectMapper.readValue(dataFromDaasString,
					// class1);
					System.out.println("recordId: " + recordId);
					updateResult = updateData(daas, config, sessionInfo, modelType, recordId, dataString);
				} else {
					System.out.println("NO RECORD FOUND");
					return updateResult;
				}
			}
		} catch (ClientProtocolException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (NullPointerException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return updateResult;
	}

	private boolean updateData(PersistenceClient daas, PersistenceConfig config, String sessionInfo, String modelType,
			String recordId, String updateDataString) {
		boolean result = false;

		String[] tokens = recordId.split(":");
		String recordIdSimplified = tokens[tokens.length - 1];
		try {
			UpdateRecordParams params = new UpdateRecordParams(sessionInfo, config.appName)
					.withId_required(recordIdSimplified).withModelType_required(modelType)
					.withUrlPrefix_required(config.persistConnInfo.host).withRestParams_required(config.url_updateOne);
			System.out.println("SIMPLIFIED = " + recordIdSimplified);
			params.setPersistData(updateDataString);
			result = daas.updateRecord(config, params);
			LOG.info("Persisted in " + modelType + " : " + result);
			System.out.println("Persisted in " + modelType + " : " + result);

		} catch (PersistDataException | NullPointerException | IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return result;
	}

	public <T> Object getData(String url, Class<T> class1) {
		URL obj;
		Object resultObject = null;
		try {
			obj = new URL(url);
			HttpURLConnection con = (HttpURLConnection) obj.openConnection();
			int responseCode = con.getResponseCode();
			System.out.println("\nSending 'GET' request to URL : " + url);
			System.out.println("Response Code : " + responseCode);

			BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
			String response = in.readLine();
			in.close();

			if (response != null) {
				// convert response to List
				JSONObject resultJson = new JSONObject(response);
				Boolean success = resultJson.getBoolean("success");
				if (success) {
					// get logical data records
					JSONObject records = resultJson.getJSONObject("dataset");
					JSONArray jsonArray = records.getJSONArray("_logical_data_records");
					JSONObject dataModelDaas = (JSONObject) jsonArray.get(0);
					dataModelDaas = removeDaasFields(dataModelDaas);
					ObjectMapper objectMapper = new ObjectMapper();
					String dataModelStringDaas = dataModelDaas.toString();
					resultObject = objectMapper.readValue(dataModelStringDaas, class1);
				}
			}
			System.out.println(response.toString());
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		return resultObject;
	}

	public <T> Object queryDataModelByCriteria(String queryData, Class<T> class1) throws Exception {

		String sessionInfo = UUID.randomUUID().toString();
		SelectRecordByCriteriaParams params = new SelectRecordByCriteriaParams(sessionInfo, config.persistConnInfo.host,
				config.url_selectCriteria, config.appName);
		params.setQuery(queryData);

		String daasResult;
		Object resultObject = null;

		try {
			daasResult = daas.selectRecordbyCriteria(config, params);
			if (daasResult != null) {
				// convert response to List
				JSONObject resultJson = new JSONObject(daasResult);
				Boolean success = resultJson.getBoolean("success");
				if (success) {
					// get logical data records
					JSONObject records = resultJson.getJSONObject("dataset");
					JSONArray jsonArray = records.getJSONArray("_logical_data_records");
					JSONObject dataModelDaas = (JSONObject) jsonArray.get(0);
					dataModelDaas = removeDaasFields(dataModelDaas);
					ObjectMapper objectMapper = new ObjectMapper();
					String dataModelStringDaas = dataModelDaas.toString();
					resultObject = objectMapper.readValue(dataModelStringDaas, class1);
				}
			}
		} catch (ClientProtocolException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (NullPointerException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		// return eventData;
		return resultObject;
	}

	/**
	 * Method to remove DAAS auto generated fields from query result
	 * 
	 * @param daasResultObject
	 * @return
	 */
	private static JSONObject removeDaasFields(JSONObject daasResultObject) {
		daasResultObject.remove("flaggedForDelete");
		daasResultObject.remove("recordId");
		daasResultObject.remove("retainDateStateSet");
		daasResultObject.remove("retainDate");
		return daasResultObject;
	}

	/* Read and return the properties object for the given config file */
	public Properties getPropValues(String configFileName) throws IOException {

		Properties prop = new Properties();

		InputStream inputStream = getClass().getClassLoader().getResourceAsStream(configFileName);

		if (inputStream != null) {
			prop.load(inputStream);
		} else {
			throw new FileNotFoundException("property file '" + configFileName + "' not found in the classpath");
		}

		return prop;
	}
}
