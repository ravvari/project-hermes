package com.cccis.nitro.HermesMessaging;

import org.redisson.api.RedissonClient;

import com.cccis.nitro.HermesMessaging.health.TemplateHealthCheck;
import com.cccis.nitro.HermesMessaging.resources.MessagesResource;
import com.cccis.nitro.HermesMessaging.resources.SubscriptionResource;
import com.cccis.nitro.HermesMessaging.resources.TopicsResource;
import com.cccis.nitro.HermesMessaging.utils.RedissonUtils;

import io.dropwizard.Application;
import io.dropwizard.setup.Bootstrap;
import io.dropwizard.setup.Environment;

/**
 * 
 * @author ravvari
 *
 */
public class HermesMessagingApplication extends Application<HermesMessagingConfiguration> {

	// private final Logger log = LoggerFactory.getLogger(getClass());

	private RedissonClient redisson;
	private RedissonUtils redissonUtils;

	public static void main(String[] args) throws Exception {
		new HermesMessagingApplication().run(args);
	}

	@Override
	public void run(HermesMessagingConfiguration config, Environment environment) throws Exception {
		final TemplateHealthCheck healthCheck = new TemplateHealthCheck();
		environment.healthChecks().register("template", healthCheck);

		TopicsResource topicResource = new TopicsResource();
		redisson = redissonUtils.createInstance(config.getRedisAddress());
		SubscriptionResource subscriptionResource = new SubscriptionResource(redisson);
		MessagesResource messagesResource = new MessagesResource(config.getTopicName(), config.getUri());

		environment.jersey().register(topicResource);
		environment.jersey().register(subscriptionResource);
		environment.jersey().register(messagesResource);
	}

	@Override
	public String getName() {
		return "HermesMessaging";
	}

	@Override
	public void initialize(final Bootstrap<HermesMessagingConfiguration> bootstrap) {
		this.redissonUtils = new RedissonUtils();
	}
}