package com.cccis.nitro.HermesMessaging;

import io.dropwizard.Configuration;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.hibernate.validator.constraints.*;

public class HermesMessagingConfiguration extends Configuration {
	@NotEmpty
	private String topicName;
	@NotEmpty
	private String uri;
	@NotEmpty
	private String redisAddress;

	@JsonProperty
	public String getTopicName() {
		return topicName;
	}

	@JsonProperty
	public void setTopicName(String topicName) {
		this.topicName = topicName;
	}

	@JsonProperty
	public String getUri() {
		return uri;
	}

	@JsonProperty
	public void setUri(String uri) {
		this.uri = uri;
	}

	@JsonProperty
	public String getRedisAddress() {
		return redisAddress;
	}

	@JsonProperty
	public void setRedisAddress(String redisAddress) {
		this.redisAddress = redisAddress;
	}

}
