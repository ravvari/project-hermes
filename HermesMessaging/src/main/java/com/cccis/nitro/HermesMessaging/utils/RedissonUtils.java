package com.cccis.nitro.HermesMessaging.utils;

import org.redisson.Redisson;
import org.redisson.api.RedissonClient;
import org.redisson.codec.SerializationCodec;
import org.redisson.config.Config;

public class RedissonUtils {

	protected static RedissonClient redisson;

	public RedissonClient getRedisson() {
		return redisson;
	}

	public Config createConfig(String redisAddresses) {
		Config config = new Config();
		config.useElasticacheServers().addNodeAddress(redisAddresses);
		config.setCodec(new SerializationCodec());
		return config;
	}

	public RedissonClient createInstance(String redisAddresses) {
		redisson = Redisson.create(createConfig(redisAddresses));
		return redisson;
	}

	void shutDown() {
		redisson.shutdown();
	}
}
