package com.cccis.nitro.HermesMessaging.resources;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.redisson.api.RMap;
import org.redisson.api.RSetMultimapCache;
import org.redisson.api.RedissonClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.cccis.nitro.HermesMessaging.utils.JSONUtils;
import com.cccis.nitro.HermesMessaging.utils.PersisterUtils;
import com.cccis.nitro.hermes.data.model.EndpointModel;
import com.cccis.nitro.hermes.data.model.SubscriptionDataModel;
import com.cccis.nitro.hermes.data.model.TopicDataModel;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.cccis.nitro.HermesMessaging.utils.HermesMessagingResponse;

/**
 * 
 * @author ravvari
 *
 */
@Path("/subscription")
public class SubscriptionResource {

	private static final String SUBSCRIPTION_DATA_MODEL = "subscription-data-model";
	private PersisterUtils persisterUtils;
	private static final String QUERY_SUBSCRIPTION_BY_ID = "{\"from\":[\"subscription-data-model\"],\"limit\":[{\"id\":{\"eq\":\"%s\"}}]}";

	ObjectMapper mapper = new ObjectMapper();
	private RedissonClient redissonClient;
	RSetMultimapCache<String, TopicDataModel> map;

	private static final Logger LOG = LoggerFactory.getLogger(TopicsResource.class);

	public SubscriptionResource(RedissonClient redissonClient) throws Exception {
		this.redissonClient = redissonClient;
		this.persisterUtils = new PersisterUtils();
	}

	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	public Response addSubscription(String subscription, @Context HttpServletRequest request) {
		HermesMessagingResponse response = new HermesMessagingResponse();
		SubscriptionDataModel subscriptionData = null;
		if (!JSONUtils.isJSONValid(subscription)) {
			response.setSuccess(false);
			response.setMessage("Error in processing the request : Invalid JSON");
			return Response.status(500).entity(response).build();
		}
		try {
			subscriptionData = mapper.readValue(subscription, SubscriptionDataModel.class);
			LOG.info("Received subscription data :" + subscription);
			persisterUtils.persistData(SUBSCRIPTION_DATA_MODEL, subscription);
			System.out.println("persisted subscription data : " + subscription);
		} catch (Exception e) {
			LOG.error("Error in persisting subscription :" + subscription);
			System.out.println("Error in persisting subscription :" + subscription);
		}

		RMap<String, List<EndpointModel>> topicSubscriberMap = redissonClient.getMap("topicSubscriberEndPoint");
		for (String topicId : subscriptionData.getTopicIds()) {
			if (topicSubscriberMap.containsKey(topicId)) {
				topicSubscriberMap.get(topicId).addAll(subscriptionData.getEndpoints());
			} else {
				topicSubscriberMap.put(topicId, subscriptionData.getEndpoints());
			}
		}
		response.setSuccess(true);
		response.setMessage("subscription created with given details");
		return Response.status(200).entity(response).build();
	}

	@GET
	@Path("/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getTopic(@PathParam("id") String id) {
		HermesMessagingResponse response = new HermesMessagingResponse();
		String query = String.format(QUERY_SUBSCRIPTION_BY_ID, id);
		SubscriptionDataModel subscriptionDetails = new SubscriptionDataModel();
		try {
			subscriptionDetails = (SubscriptionDataModel) persisterUtils.queryDataModelByCriteria(query,
					SubscriptionDataModel.class);
		} catch (Exception e) {
			String error = "Error in retrieving subscription details with id: " + id;
			LOG.error(error);
			System.out.println(error);
		}

		response.setSuccess(true);
		response.setMessage("subscription details  successfully retrieved");
		response.setContent(subscriptionDetails);
		return Response.status(200).entity(response).build();
	}

	@PUT
	@Path("/{id}")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response updateSubscriber(@PathParam("id") String id, String updatedSubscriberDetails,
			@Context HttpServletRequest request) {
		HermesMessagingResponse response = new HermesMessagingResponse();
		String query = String.format(QUERY_SUBSCRIPTION_BY_ID, id);

		if (!JSONUtils.isJSONValid(updatedSubscriberDetails)) {
			response.setSuccess(false);
			response.setMessage("Error in processing the request : Invalid JSON");
			return Response.status(500).entity(response).build();
		}
		try {
			LOG.info("Received update request for topic with data :" + updatedSubscriberDetails);
			persisterUtils.updateData(SUBSCRIPTION_DATA_MODEL, query, updatedSubscriberDetails,
					SubscriptionDataModel.class);
			System.out.println("Updated subscription data : " + updatedSubscriberDetails);
		} catch (Exception e) {
			String error = "Error in updating subscription :" + updatedSubscriberDetails;
			LOG.error(error);
			response.setSuccess(false);
			response.setMessage(error);
			response.setContent(e.getMessage());
			System.out.println(error);
			return Response.status(500).entity(response).build();
		}

		response.setSuccess(true);
		response.setMessage("Topic updated with given details");
		return Response.status(200).entity(response).build();

	}

}